package com.althof.eCommercePart1.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException {
    private String resourcesName;
    private String fieldName;
    private Object fieldValue;

    public ResourceNotFoundException(String resourcesName, String fieldName, Object fieldValue){
        super(String.format("%s not found with %s : '%s'",resourcesName,fieldName,fieldValue));
        this.resourcesName = resourcesName;
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
    }

    public String getResourcesName() {
        return resourcesName;
    }

    public String getFieldName(){
        return fieldName;
    }

    public Object getFieldValue(){
        return fieldValue;
    }
}
