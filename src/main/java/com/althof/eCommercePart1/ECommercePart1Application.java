package com.althof.eCommercePart1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class ECommercePart1Application {

	public static void main(String[] args) {
		SpringApplication.run(ECommercePart1Application.class, args);
	}

}
