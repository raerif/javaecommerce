package com.althof.eCommercePart1.controller;

import com.althof.eCommercePart1.exception.ResourceNotFoundException;
import com.althof.eCommercePart1.model.Product;
import com.althof.eCommercePart1.repository.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api")
public class ProductController {

    @Autowired
    ProductRepo productRepo;

    // Get All Product test
    @GetMapping("/products")
    public List<Product> getAllProducts(){
        return productRepo.findAll();
    }

    // Create new product
    @PostMapping("/products")
    public Product createProduct(@Valid @RequestBody Product product){
        return productRepo.save(product);
    }

    // Get the product by id
    @GetMapping("/products/{id}")
    public Product getProductById(@PathVariable(value = "id") Long id){
        return productRepo.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Product", "id", id));
    }

    // Update the product
    @PutMapping("/products/{id}")
    public Product updateProduct(@PathVariable(value = "id") Long id, @Valid @RequestBody Product productDetails){
        Product product = productRepo.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Product", "id", id));
        product.setProductCode(productDetails.getProductCode());
        product.setProductName(productDetails.getProductName());
        product.setProductPrice(productDetails.getProductPrice());
        product.setBrandName(productDetails.getBrandName());
        product.setCategoryNames(productDetails.getCategoryNames());
        product.setStoreName(productDetails.getStoreName());
        product.setProductDesc(productDetails.getProductDesc());

        Product updatedProduct = productRepo.save(product);
        return updatedProduct;
    }

    // Delete products
    @DeleteMapping("/products/{id}")
    public ResponseEntity<?> deleteProduct(@PathVariable(value = "id") Long id){
        Product product = productRepo.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Product", "id", id));
        productRepo.delete(product);

        return ResponseEntity.ok().build();
    }
}
